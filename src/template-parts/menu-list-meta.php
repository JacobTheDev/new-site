<?php
$args = array_merge([
    "post"      => null,
    "class"     => [
        "nav"  => "",
        "list" => "",
    ],
    "light"      => false,
    "permalink"  => "",
    "datetime"   => null,
    "author"     => null,
    "comments"   => null,
    "taxonomies" => null,
], $args);

/**
 * Retrieve values based on `$post` if set
 */
if ($args["post"] instanceof WP_Post) {
    /**
     * Retrieve datetime
     */
    if ($args["datetime"] === null) {
        $args["datetime"] = new DateTime(get_the_time("c", $args["post"]->ID));
    }

    /**
     * Retrieve author
     */
    if ($args["author"] === null) {
        $author_id = get_post_field("post_author", $args["post"]->ID);

        $args["author"] = [
            "name" => get_the_author_meta("display_name", $author_id),
            "url"  => get_author_posts_url($author_id),
        ];
    }

    /**
     * Retrieve comments
     */
    if ($args["comments"] === "null") {
        $args["comments"]["count"] = [
            "count" => get_comments_number($args["post"]->ID),
            "url"   => get_comments_link($args["post"]->ID),
        ];
    }

    /**
     * Retrieve taxonomies
     */
    if ($args["taxonomies"] === null) {
        $args["taxonomies"] = [
            [
                "icon"   => "fa-solid fa-folder",
                "prefix" => __("Posted in:", "__gulp_init_namespace__"),
                "terms"  => get_the_terms($args["post"]->ID, "category"),
            ],
            [
                "icon"   => "fa-solid fa-tag",
                "prefix" => __("Tagged with:", "__gulp_init_namespace__"),
                "terms"  => get_the_terms($args["post"]->ID, "post_tag"),
            ],
        ];
    }
}
?>

<?php if ($args["datetime"] || $args["author"] || $args["comments"] || $args["taxonomies"]): ?>
    <nav class="<?php echo esc_attr(trim("menu-list__container {$args["class"]["nav"]}")); ?>">
        <ul class="<?php echo esc_attr(trim("menu-list menu-list--meta {$args["class"]["list"]}")); ?><?php if ($light): ?> __light<?php endif; ?>">

            <?php if ($args["datetime"]): ?>
                <li class="menu-list__item">

                    <i class="menu-list__icon fa-solid fa-clock"></i>

                    <?php if ($args["permalink"]): ?>
                        <a class="menu-list__link link" href="<?php echo esc_url($args["permalink"]); ?>">
                    <?php endif; ?>

                    <time class="menu-list__time" datetime="<?php echo esc_attr($args["datetime"]->format("c")); ?>">

                        <span class="__visuallyhidden">
                            <?php _e("Posted on", "__gulp_init_namespace__"); ?>
                        </span>

                        <?php echo $args["datetime"]->format(get_option("date_format")); ?>

                    </time>

                    <?php if ($args["permalink"]): ?>
                        </a><!--/.menu-list__link-->
                    <?php endif; ?>

                </li><!--/.menu-list__item-->
            <?php endif; // ($args["datetime"]) ?>

            <?php if ($args["author"]): ?>
                <li class="menu-list__item">

                    <i class="menu-list__icon fa-solid fa-circle-user"></i>

                    <a class="menu-list__link link" href="<?php echo esc_url($args["author"]["url"]); ?>">

                        <span class="__visuallyhidden">
                            <?php _e("Written by", "__gulp_init_namespace__"); ?>
                        </span>

                        <?php echo $args["author"]["name"]; ?>

                    </a>

                </li><!--/.menu-list__item-->
            <?php endif; // ($args["author"]) ?>

            <?php if ($args["comments"]): ?>
                <li class="menu-list__item">

                    <i class="menu-list__icon fa-solid fa-comment"></i>

                    <?php if ($args["comments"]["url"]): ?>
                        <a class="menu-list__link link" href="<?php echo esc_url($arg["comments"]["url"]); ?>">
                    <?php endif; ?>

                    <?php printf(_n("%s comment", "%s comments", $args["comments"]["count"], "__gulp_init_namespace__"), number_format_i18n($args["comments"]["count"])); ?>

                    <?php if ($args["comments"]["url"]): ?>
                        </a>
                    <?php endif; ?>

                </li><!--/.menu-list__item-->
            <?php endif; // ($args["comments"]) ?>

            <?php if ($args["taxonomies"]): ?>
                <?php foreach ($args["taxonomies"] as $taxonomy): $i = 0; ?>
                    <?php if ($taxonomy["terms"]): $i = 0; ?>

                        <?php $term_count = count($taxonomy["terms"]); ?>

                        <li class="menu-list__item">

                            <i class="menu-list__icon <?php echo esc_attr($taxonomy["icon"]); ?>"></i>

                            <span class="__visuallyhidden">
                                <?php echo $taxonomy["prefix"]; ?>
                            </span>

                            <?php foreach ($taxonomy["terms"] as $term): $i++; ?>
                                <a class="menu-list__link link" href="<?php echo esc_url(get_term_link($term->term_id, $term->taxonomy)); ?>"><?php echo $term->name; ?></a><?php if ($i < $term_count): ?>, <?php endif; ?>
                            <?php endforeach; ?>

                        </li><!--/.menu-list__item-->

                    <?php endif; //($taxonomy["terms"]) ?>
                <?php endforeach; // ($args["taxonomies"] as $taxonomy) ?>
            <?php endif; // ($args["taxonomies"]) ?>

        </ul><!--/.menu-list-->
    </nav><!--/.article__menu-list__container-->
<?php endif; // ($args["datetime"] || $args["author"] || $args["comments"] || $args["taxonomies"]) ?>

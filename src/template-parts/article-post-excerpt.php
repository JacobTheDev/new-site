<?php
$args = array_merge([
    "post"      => null,
    "class"     => "",
    "light"     => false,
    "permalink" => "",
    "title"     => "",
    "meta"      => null,
    "excerpt"   => "",
], $args);

/**
 * Retrieve values based on `$post` if set
 */
if ($args["post"] instanceof WP_Post) {
    /**
     * Retrieve permalink
     */
    if ($args["permalink"] === "") {
        $args["permalink"] = get_permalink($args["post"]->ID);
    }

    /**
     * Set title
     */
    if ($args["title"] === "") {
        $args["title"] = apply_filters("the_title", $args["post"]->post_title, $args["post"]->ID);
    }

    /**
     * Set meta based on post type
     */
    if ($args["meta"] === null && $args["post"]->post_type === "post") {
        $args["meta"] = true;
    }

    /**
     * Set excerpt
     */
    if ($args["excerpt"] === "") {
        $args["excerpt"] =  __gulp_init_namespace___get_the_excerpt($args["post"]->ID, [
            "truncate" => [
                "count" => 55,
            ],
            "suffix"   => [
                "value"    => "&hellip;",
                "optional" => true,
            ],
        ]);
    }
}
?>

<?php if ($args["title"] || $args["permalink"]): ?>
    <article class="<?php echo esc_attr(trim("article article--post-excerpt {$args["class"]}")); ?>">

        <?php if ($args["title"] || $args["meta"]): ?>
            <header class="article__header">

                <?php if ($args["title"]): ?>
                    <h3 class="article__title title<?php if ($args["light"]): ?> __light<?php endif; ?>">

                        <?php if ($args["permalink"]): ?>
                            <a class="title__link link<?php if ($args["light"]): ?> link--inherit<?php endif; ?>" href="<?php echo esc_url($args["permalink"]); ?>">
                        <?php endif; ?>

                        <?php echo $args["title"]; ?>

                        <?php if ($args["permalink"]): ?>
                            </a>
                        <?php endif; ?>

                    </h3><!--/.article__title-->
                <?php endif; // ($args["title"]) ?>

                <?php
                if ($args["meta"]) {
                    get_template_part("template-parts/menu-list", "meta", [
                        "post"  => $args["post"],
                        "class" => "article__menu-list__container",
                        "light" => $args["light"],
                    ]);
                }
                ?>

            </header><!--/.article__header-->
        <?php endif; // ($args["title"] || $args["meta"]) ?>

        <?php if ($args["excerpt"]): ?>
            <div class="article__content">
                <p class="article__text text<?php if ($args["light"]): ?> __light<?php endif; ?>">
                    <?php echo $args["excerpt"]; ?>
                    <?php if ($args["permalink"]): ?>
                        <a class="text__link link<?php if ($args["light"]): ?> link--inherit<?php endif; ?>" href="<?php echo esc_url($args["permalink"]); ?>">
                            <?php _e("Read More", "__gulp_init_namespace__"); ?>
                        </a>
                    <?php endif; ?>
                </p>
            </div><!--/.article__content-->
        <?php endif; ?>

    </article><!--/.article-->
<?php endif; // ($args["title"] || $args["permalink"]) ?>

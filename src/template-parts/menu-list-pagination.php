<?php
$args = array_merge([
    "class"      => [
        "nav"  => "",
        "list" => "",
    ],
    "links" => paginate_links(["type" => "array"]),
], $args);
?>
<?php if ($args["links"]): ?>
    <nav class="<?php echo esc_attr(trim("menu-list__container {$args["class"]["nav"]}")); ?>">
        <ul class="<?php echo esc_attr(trim("menu-list menu-list--pagination {$args["class"]["list"]}")); ?>">
            <?php foreach ($args["links"] as $link): ?>
                <li class="menu-list__item">
                    <?php echo apply_filters("__gulp_init_namespace___menu_list_link", $link); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </nav>
<?php endif; ?>

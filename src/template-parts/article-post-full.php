<?php
$args = array_merge([
    "post"    => null,
    "class"   => "",
    "light"   => false,
    "title"   => "",
    "meta"    => null,
    "content" => "",
], $args);

/**
 * Retrieve values based on `$post` if set
 */
if ($args["post"] instanceof WP_Post) {
    /**
     * Set title
     */
    if ($args["title"] === "") {
        $args["title"] = apply_filters("the_title", $args["post"]->post_title, $args["post"]->ID);
    }

    /**
     * Set meta based on post type
     */
    if ($args["meta"] === null && $args["post"]->post_type === "post") {
        $args["meta"] = true;
    }

    /**
     * Set content
     */
    if ($args["content"] === "") {
        $args["content"] = apply_filters("the_content", $args["post"]->post_content);
    }
}
?>

<?php if ($args["title"] || $args["content"]): ?>
    <article class="<?php echo esc_attr(trim("article article--post-full {$args["class"]}")); ?>">

        <?php if ($args["title"] || $args["meta"]): ?>
            <header class="article__header">

                <?php if ($args["title"]): ?>
                    <h1 class="article__title title<?php if ($args["light"]): ?> __light<?php endif; ?>">
                        <?php echo $args["title"]; ?>
                    </h1>
                <?php endif; ?>

                <?php if ($args["meta"]): ?>
                    <?php
                    get_template_part("template-parts/menu-list", "meta", [
                        "post"  => $args["post"],
                        "class" => "article__menu-list__container",
                        "light" => $args["light"],
                    ]);
                    ?>
                <?php endif; ?>

            </header><!--/.article_header-->
        <?php endif; ?>

        <?php if ($args["content"]): ?>
            <div class="article__content">
                <div class="article__user-content user-content<?php if ($args["light"]): ?> user-content--light<?php endif; ?>">
                    <?php echo $args["content"]; ?>
                </div>
            </div>
        <?php endif; ?>

    </article><!--/.article-->
<?php endif; // ($args["title"] || $args["content"]) ?>

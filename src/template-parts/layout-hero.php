<?php
$args = array_merge([
    "post"       => null,
    "class"      => [
        "block"  => "",
        "inner"  => "",
        "swiper" => "",
    ],
    "image_size" => "hero",
    "pagination" => false,
    "navigation" => false,
    "slideshow"  => null,
    "image"      => null,
    "title"      => "",
], $args);

/**
 * Retrieve values based on `$post` if set
 */
if ($args["post"] instanceof WP_Post) {
    /**
     * Retrieve slideshow
     */
    if ($args["slideshow"] === null) {
        $args["slideshow"] = __gulp_init_namespace___get_field("slideshow", $args["post"]->ID);
    }

    /**
     * Retrieve image
     */
    if ($args["image"] === null && has_post_thumbnail($args["post"])) {
        $args["image"] = [
            "alt" => get_post_meta(get_post_thumbnail_id($args["post"]->ID), "_wp_attachment_image_alt", true),
            "sizes" => [
                "small"  => wp_get_attachment_image_src(get_post_thumbnail_id($args["post"]->ID), "{$args["image_size"]}")[0],
                "medium" => wp_get_attachment_image_src(get_post_thumbnail_id($args["post"]->ID), "{$args["image_size"]}_medium")[0],
                "large"  => wp_get_attachment_image_src(get_post_thumbnail_id($args["post"]->ID), "{$args["image_size"]}_large")[0],
            ],
        ];
    }

    /**
     * Set title
     */
    if ($args["title"] === "" && $args["post"]->post_title) {
        $args["title"] = apply_filters("the_title", $args["post"]->post_title, $args["post"]->ID);
    }
}
?>
<?php if ($args["slideshow"] || $args["image"]): ?>
    <div class="<?php echo esc_attr(trim("hero-block {$args["class"]["block"]}")); ?>" role="region">
        <div class="<?php echo esc_attr(trim("hero__inner {$args["class"]["inner"]}")); ?>">
            <div class="<?php echo esc_attr(trim("hero__swiper-container swiper-container {$args["class"]["swiper"]}")); ?>">

                <div class="swiper-wrapper">
                    <?php if ($args["slideshow"]): ?>
                        <?php foreach ($args["slideshow"] as $key => $slide): ?>

                            <?php
                            $image = isset($slide["image"]) ? $slide["image"] : false;
                            $link  = isset($slide["link"]) ? $slide["link"] : false;
                            ?>

                            <?php if ($image): ?>
                                <figure class="swiper-slide">

                                    <?php if ($link && $link["url"]): ?>
                                        <a class="swiper__link link" href="<?php echo $link["url"]; ?>"<?php if ($link["title"]): ?> title="<?php echo $link["title"]; ?>"<?php endif; ?><?php if ($link["target"]): ?> rel="noopener noreferrer" target="<?php echo $link["target"]; ?>"<?php endif; ?>>
                                    <?php endif; ?>

                                    <?php if ($image["sizes"]["{$args["image_size"]}"]): ?>
                                        <picture class="swiper__picture">

                                            <?php if ($image["sizes"]["{$args["image_size"]}_large"]): ?>
                                                <?php echo __gulp_init_namespace___img($image["sizes"]["{$args["image_size"]}_large"], ["media" => "(min-width: 64em)"], $key > 0, "source"); ?>
                                            <?php endif; ?>

                                            <?php if ($image["sizes"]["{$args["image_size"]}_medium"]): ?>
                                                <?php echo __gulp_init_namespace___img($image["sizes"]["{$args["image_size"]}_medium"], ["media" => "(min-width: 40em)"], $key > 0, "source"); ?>
                                            <?php endif; ?>

                                            <?php if ($image["sizes"]["{$args["image_size"]}"]): ?>
                                                <?php echo __gulp_init_namespace___img($image["sizes"]["{$args["image_size"]}"], ["alt" => $image["alt"], "class" => "swiper__image" . ($key > 0 ? " swiper-lazy" : "")], $key > 0); ?>
                                            <?php endif; ?>

                                        </picture><!--/.swiper__picture-->
                                    <?php endif; // ($image["sizes"]["{$args["image_size"]}"]) ?>

                                    <?php if ($image["title"] || $image["caption"]): ?>
                                        <figcaption class="swiper__caption">
                                            <div class="swiper__caption__inner">

                                                <?php if ($image["title"]): ?>
                                                    <h6 class="swiper__title title<?php echo ! $image["caption"] ? " __nomargin" : ""; ?>">
                                                        <?php echo $image["title"]; ?>
                                                    </h6>
                                                <?php endif; ?>

                                                <?php if ($image["caption"]): ?>
                                                    <div class="swiper__user-content user-content user-content--light">
                                                        <?php echo apply_filters("the_content", $image["caption"]); ?>
                                                    </div>
                                                <?php endif; ?>

                                            </div><!--/.swiper__caption__inner-->
                                        </figcaption><!--/.swiper__caption-->
                                    <?php endif; // ($image["title"] || $image["caption"]) ?>

                                    <?php if ($link && $link["url"]): ?>
                                        </a><!--/.swiper__link-->
                                    <?php endif; ?>

                                </figure><!--/.swiper-slide-->
                            <?php endif; // ($image) ?>

                        <?php endforeach; // ($slideshow as $slide) ?>
                    <?php elseif ($args["image"]): ?>
                        <figure class="swiper-slide">

                            <picture class="swiper__picture">

                                <?php if ($args["image"]["sizes"]["large"]): ?>
                                    <?php echo __gulp_init_namespace___img($args["image"]["sizes"]["large"], ["media" => "(min-width: 64em)"], false, "source"); ?>
                                <?php endif; ?>

                                <?php if ($args["image"]["sizes"]["medium"]): ?>
                                    <?php echo __gulp_init_namespace___img($args["image"]["sizes"]["medium"], ["media" => "(min-width: 40em)"], false, "source"); ?>
                                <?php endif; ?>

                                <?php if ($args["image"]["sizes"]["small"]): ?>
                                    <?php echo __gulp_init_namespace___img($args["image"]["sizes"]["small"], ["alt" => $args["image"]["alt"], "class" => "swiper__image"], false); ?>
                                <?php endif; ?>

                            </picture><!--/.swiper-picture-->

                            <?php if ($args["title"]): ?>
                                <header class="swiper__caption">
                                    <div class="swiper__caption__inner">
                                        <h1 class="swiper__title title __nomargin" role="heading">
                                            <?php echo $args["title"]; ?>
                                        </h1>
                                    </div>
                                </header>
                            <?php endif; ?>

                        </figure><!--/.swiper-slide-->
                    <?php endif; // ($args["slideshow"]) elseif ($args["image"]) ?>
                </div><!--/.swiper-wrapper-->

                <?php if ($args["slideshow"] && ($args["pagination"] || $args["navigation"]) && count($args["slideshow"]) > 1): ?>

                    <?php if ($args["pagination"]): ?>
                        <div class="swiper-pagination"></div>
                    <?php endif; ?>

                    <?php if ($args["navigation"]): ?>

                        <button class="swiper-button swiper-button--prev">
                            <i class="swiper-button__icon fa-solid fa-caret-left"></i>
                            <span class="__visuallyhidden"><?php _e("Previous Slide", "__gulp_init_namespace__"); ?></span>
                        </button>

                        <button class="swiper-button swiper-button--next">
                            <i class="swiper-button__icon fa-solid fa-caret-right"></i>
                            <span class="__visuallyhidden"><?php _e("Next Slide", "__gulp_init_namespace__"); ?></span>
                        </button>

                    <?php endif; // ($args["navigation"]) ?>

                <?php endif; // ($args["slideshow"] && ($args["pagination"] || $args["navigation"]) && count($args["slideshow"]) > 1) ?>

            </div><!--/.hero__swiper-container-->
        </div><!--/.hero__inner-->
    </div><!--/.hero-block-->
<?php endif; //  ($args["slideshow"] || $args["image"]) ?>

<?php
$args = array_merge([
    "class" => "",
    "light" => false,
    "error" => "",
], $args);
?>

<?php if ($args["error"]): ?>
    <article class="<?php echo esc_attr(trim("article article--post-none {$args["class"]}")); ?>">
        <div class="article__content">
            <p class="article__text text<?php if ($args["light"]): ?> __light<?php endif; ?>"><?php echo $args["error"]; ?></p>
        </div>
    </article>
<?php endif; ?>

<?php
$args = array_merge([
    "class"   => [
        "block" => "",
        "inner" => "",
    ],
    "content" => __gulp_init_namespace___get_field("content", "alert"),
], $args);
?>

<?php if (trim(strip_tags($args["content"]))): ?>
    <div class="<?php echo esc_attr(trim("alert-block {$args["class"]["block"]}")); ?>">
        <div class="<?php echo esc_attr(trim("alert__inner {$args["class"]["inner"]}")); ?>">
            <div class="alert__row row row--padded row--direction-reverse">

                <div class="col-12 col-xs-auto col--grow-0 col--shrink-0 __textright">
                    <button class="alert__button">
                        <i class="button__icon fa-solid fa-circle-xmark fa-fw"></i>
                        <span class="__visuallyhidden"><?php _e("Dismiss Alert", "__gulp_init_namespace__"); ?></span>
                    </button>
                </div>

                <div class="col-12 col-xs-0" style="align-self:center;">
                    <div class="alert__user-content user-content user-content--light">
                        <?php echo $args["content"]; ?>
                    </div>
                </div>

            </div><!--/.alert__row-->
        </div><!--/.alert__inner-->
    </div><!--/.alert-block-->
<?php endif; // ($args["content"]) ?>

// JavaScript Document

// Scripts written by __gulp_init_author_name__ @ __gulp_init_author_company__

import "lazysizes";
import "lazysizes/plugins/native-loading/ls.native-loading";

document.addEventListener("lazybeforeunveil", (e) => {
    const IMG = e.target.getAttribute("data-bg");

    if (IMG) {
        e.target.style.backgroundImage = `url('${IMG}')`;
    }
});

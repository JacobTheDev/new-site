// JavaScript Document

// Scripts written by __gulp_init_author_name__ @ __gulp_init_author_company__

document.querySelectorAll("[id^='acwp-toggler-'").forEach((input) => {
    if (sessionStorage.getItem(input.id) === "true" && input.checked !== true) {
        input.click();
    }

    input.addEventListener("change", () => {
        sessionStorage.setItem(input.id, input.checked);
    });
});

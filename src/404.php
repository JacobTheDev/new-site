<!doctype html>
<html class="no-javascript" <?php language_attributes(); ?>>
    <head>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div class="page__container" id="page-container">
            <main id="content" tabindex="0">
                <div class="content-block">
                    <div class="content__inner">
                        <div class="content__post">
                            <?php do_action("__gulp_init_namespace___before_content"); ?>

                            <a class="content__logo logo" href="<?php echo esc_url(get_bloginfo("url")); ?>" style="margin:0 auto 2em;max-width:300px;">
                                <?php echo __gulp_init_namespace___img(get_theme_file_uri("assets/media/logo.svg"), ["alt" => get_bloginfo("name"), "class" => "logo__image", "height" => "69", "width" => "300"], false); ?>
                            </a>

                            <article class="content__article article">
                                <header class="article__header">
                                    <h1 class="article__title title __textcenter"><?php _e("404: Page Not Found", "__gulp_init_namespace__"); ?></h1>
                                </header>
                                <div class="article__content">
                                    <p class="article__text text __textcenter"><?php _e("This page could not be found. It may have been moved or deleted.", "__gulp_init_namespace__"); ?></p>
                                </div>
                            </article>

                            <p class="content__text text __textcenter __nomargin">
                                <?php printf(__("© Copyright %s %s", "__gulp_init_namespace__"), wp_date("Y"), get_bloginfo("name")); ?>
                            </p>

                            <?php do_action("__gulp_init_namespace___after_content"); ?>
                        </div><!--/.content__post-->
                    </div><!--/.content__inner-->
                </div><!--/.content-block-->
            </main><!--/#content-->
        </div><!--/.page__container-->
        <?php wp_footer(); ?>
    </body>
</html>

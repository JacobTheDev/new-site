<?php
/* ------------------------------------------------------------------------ *\
 * Meta
\* ------------------------------------------------------------------------ */

/**
 * Add the favicon meta tags to the head
 *
 * @return void
 */
function __gulp_init_namespace___add_favicon_meta_to_head(): void {
    echo "<link href='" . get_theme_file_uri("assets/media/logo-favicon.png") . "' rel='icon' sizes='any' />\n";
    echo "<link href='" . get_theme_file_uri("assets/media/logo-favicon.svg") . "' rel='icon' type='image/svg+xml' />\n";
}
add_action("admin_head", "__gulp_init_namespace___add_favicon_meta_to_head", 0);
add_action("login_head", "__gulp_init_namespace___add_favicon_meta_to_head", 0);
add_action("wp_head", "__gulp_init_namespace___add_favicon_meta_to_head", 0);

/**
 * Add the theme color meta tag to the head
 *
 * @return void
 */
function __gulp_init_namespace___add_theme_color_meta_to_head(): void {
    echo "<meta name='theme-color' content='" . (__gulp_init_namespace___get_field("theme_color", "pwa") ?: "<%= pwa_theme_color %>") . "' />\n";
}
add_action("admin_head", "__gulp_init_namespace___add_theme_color_meta_to_head", 0);
add_action("login_head", "__gulp_init_namespace___add_theme_color_meta_to_head", 0);
add_action("wp_head", "__gulp_init_namespace___add_theme_color_meta_to_head", 0);

/**
 * Add the settings meta tags to the head
 *
 * @return void
 */
function __gulp_init_namespace___add_settings_meta_to_head(): void {
    echo "<meta content='text/html;charset=utf-8' http-equiv='content-type' />\n";
    echo "<meta content='width=device-width, initial-scale=1' name='viewport' />\n";
}
add_action("wp_head", "__gulp_init_namespace___add_settings_meta_to_head", 0);

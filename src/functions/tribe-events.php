<?php
/* ------------------------------------------------------------------------ *\
 * Tribe Events
\* ------------------------------------------------------------------------ */

// stop if Tribe isn't installed
if (! function_exists("tribe_get_events")) {
    return;
}

/**
 * Check if the currently viewed page is a "Tribe" page
 *
 * @return boolean
 */
function __gulp_init_namespace__is_tribe_page(): bool {
    if (array_key_exists("wp_query", $GLOBALS) && array_key_exists("post_type", $GLOBALS["wp_query"]->query) && $GLOBALS["wp_query"]->query["post_type"] === "tribe_events") {
        return true;
    }

    return false;
}

/**
 * Change order of Tribe Events dependencies so that they work better with async/defer
 *
 * @return void
 */
function __gulp_init_namespace___tribe_events_fix_scripts_order(): void {
    global $wp_scripts;

    /**
     * Array to store transposed dependencies
     */
    $deps = [];

    /**
     * Find all `tribe-events-views-v2-` dependencies of `tribe-events-views-v2-manager`, store them
     * in a variable for reference, and remove them.
     */
    if (isset($wp_scripts->registered["tribe-events-views-v2-manager"])) {
        foreach ($wp_scripts->registered["tribe-events-views-v2-manager"]->deps as $key => $handle) {
            if (preg_match("/^tribe-events-views-v2-.+$/", $handle)) {
                $deps[] = $handle;
                unset($wp_scripts->registered["tribe-events-views-v2-manager"]->deps[$key]);
            }
        }
    }

    /**
     * Add `tribe-events-views-v2-manager` as a dependency for all dependencies
     */
    foreach ($deps as $dep) {
        if (key_exists($dep, $wp_scripts->registered)) {
            if (! in_array("tribe-events-views-v2-manager", $wp_scripts->registered[$dep]->deps)) {
                $wp_scripts->registered[$dep]->deps[] = "tribe-events-views-v2-manager";
            }
        }
    }
}
add_action("wp_enqueue_scripts", "__gulp_init_namespace___tribe_events_fix_scripts_order");

/**
 * Use simplified template for Tribe tickets
 *
 * @param string $template
 * @return string
 */
function __gulp_init_namespace___tribe_events_tickets_template_include(string $template): string {
    if (isset($_GET["tickets_provider"]) && isset($_GET["tec-tc-cookie"])) {
        $template = locate_template(["tribe/tickets/v2/attendee-registration/default-template.php", "page.php", "single.php", "index.php"]);
    }

    return $template;
}
add_filter("template_include", "__gulp_init_namespace___tribe_events_tickets_template_include", 100, 1);

/**
 * Remove content filters before inserting Tickets markup
 *
 * @return void
 */
function __gulp_init_namespace___tribe_events_tickets_reset_the_content(): void {
    if (isset($_GET["tickets_provider"]) && isset($_GET["tec-tc-cookie"])) {
        remove_filter("the_content", "__gulp_init_namespace___add_user_content_classes", 20);
        remove_filter("the_content", "__gulp_init_namespace___wrap_handorgel_shortcodes", 30);
        remove_filter("the_content", "__gulp_init_namespace___responsive_iframes", 20);
        remove_filter("the_content", "__gulp_init_namespace___responsive_tables", 20);
        remove_filter("the_content", "__gulp_init_namespace___lazy_load_images", 30);
    }
}
add_action("__gulp_init_namespace___before_content", "__gulp_init_namespace___tribe_events_tickets_reset_the_content");

/**
 * Restore content filters after inserting Tickets markup
 *
 * @return void
 */
function __gulp_init_namespace___tribe_events_tickets_restore_the_content(): void {
    if (isset($_GET["tickets_provider"]) && isset($_GET["tec-tc-cookie"])) {
        add_filter("the_content", "__gulp_init_namespace___add_user_content_classes", 20, 1);
        add_filter("the_content", "__gulp_init_namespace___wrap_handorgel_shortcodes", 30, 1);
        add_filter("the_content", "__gulp_init_namespace___responsive_iframes", 20, 1);
        add_filter("the_content", "__gulp_init_namespace___responsive_tables", 20, 1);
        add_filter("the_content", "__gulp_init_namespace___lazy_load_images", 30, 1);
    }
}
add_action("__gulp_init_namespace___after_content", "__gulp_init_namespace___tribe_events_tickets_restore_the_content");

/**
 * Remove recurring events duplicates from search results
 *
 * @see https://www.relevanssi.com/knowledge-base/showing-one-recurring-event/
 *
 * @param  array<array<mixed>> $hits Array of Relevnassi hits
 *
 * @return array<array<mixed>>
 */
function __gulp_init_namespace___tribe_events_relevanssi_cull_recurring(array $hits = []): array {
    $ok_results     = [];
    $posts_seen     = [];
    $index_by_title = [];
    $date_by_title  = [];

    $i = 0;

    foreach ($hits[0] as $hit) {
        if (! isset($posts_seen[$hit->post_title])) {
            $ok_results[]                     = $hit;
            $date_by_title[$hit->post_title]  = get_post_meta($hit->ID, "_EventStartDate", true);
            $index_by_title[$hit->post_title] = $i;
            $posts_seen[$hit->post_title]     = true;
            $i++;
        } elseif (get_post_meta($hit->ID, "_EventStartDate", true) < $date_by_title[$hit->post_title]) {
            if (strtotime(get_post_meta($hit->ID, "_EventStartDate", true)) < time()) continue;
            $date_by_title[$hit->post_title]               = get_post_meta($hit->ID, "_EventStartDate", true);
            $ok_results[$index_by_title[$hit->post_title]] = $hit;
        }
    }

    $hits[0] = $ok_results;

    return $hits;
}
add_filter("relevanssi_hits_filter", "__gulp_init_namespace___tribe_events_relevanssi_cull_recurring");

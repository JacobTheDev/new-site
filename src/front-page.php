<?php get_header(); ?>

<?php
get_template_part("template-parts/layout", "hero", [
    "post" => $post,
]);
?>

<div class="content-block">
    <div class="content__inner">
        <div class="content__post">
            <?php do_action("__gulp_init_namespace___before_content"); ?>

            <?php
            if (have_posts()) {
                while (have_posts()) { the_post();
                    get_template_part("template-parts/article", "post-full", [
                        "post"  => $post,
                        "class" => "content__article",
                        "title" => false,
                    ]);
                }
            }
            ?>

            <?php do_action("__gulp_init_namespace___after_content"); ?>
        </div><!--/.content__post-->
    </div><!--/.content__inner-->
</div><!--/.content-block-->

<?php get_footer(); ?>
